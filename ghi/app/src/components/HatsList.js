import React, { useEffect } from "react";
import { NavLink, Outlet, Link } from 'react-router-dom';


// function loadHats() {
//   const response1 = await fetch ('http://localhost:8090/api/hats/');
//   if (response1.ok) {
//     const data = await response1.json();
//     useState({hats: data.hats})
//   } else {
//     console.error(response1)
//   }
// }


export default function HatsList(props) {
    console.log("hats here!")

    if (props.hats === undefined) {
        return null;
      }
      return (
      <div style={{ display: 'flex' }}>
        <nav
          style={{
            borderRight: 'solid 1px',
            padding: '1rem',
          }}
        >
          <Link to='new'>Create A New Hat</Link>
          {props.hats.map((hat) => (
            <NavLink

              style={({ isActive }) => {
                return {
                  display: "block",
                  margin: "1rem 0",
                  color: isActive ? "red" : "",
                };
              }}
              to={`/hats/${hat.id}`}
              key={hat.id}
            >
              {hat.style_name}
            </NavLink>
          ))}
        </nav>
        <Outlet />
      </div>


      );

}
